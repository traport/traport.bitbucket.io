
# coding: utf-8

# In[1]:

import pandas as pd
import datetime
import plotly.express as px
import plotly.graph_objects as go

print('reading CSV')
df = pd.read_csv('https://covidtracking.com/api/v1/states/daily.csv',parse_dates=[0])


# In[2]:

dftn = df[df['state']=='TN']
dftn = dftn[dftn['date']>pd.Timestamp(datetime.date(2020,3,11))]

dftn['deathRollingIncr'] = dftn['deathIncrease'].rolling(7).mean()

# In[3]:

from plotly.subplots import make_subplots
fig_tn = make_subplots(specs=[[{"secondary_y": True}]])
# fig_deaths = go.Figure()
fig_tn.add_trace(go.Scatter(x=dftn['date'],y=dftn['positiveIncrease'],mode='lines+markers',name='New positive cases'))
fig_tn.add_trace(go.Scatter(x=dftn['date'],y=dftn['totalTestResultsIncrease'],mode='lines+markers',name='New total tests'))
fig_tn.add_trace(go.Scatter(x=dftn['date'],y=dftn['deathIncrease'],mode='lines+markers',name='New deaths'))
fig_tn.add_trace(go.Scatter(x=dftn['date'],y=dftn['deathRollingIncr'],mode='lines+markers',name='New deaths (7-day rolling)'))
fig_tn.add_trace(go.Scatter(x=dftn['date'],y=dftn['positiveIncrease']/dftn['totalTestResultsIncrease'],stackgroup='one',name='Positive fraction'),secondary_y=True)
fig_tn.add_trace(go.Scatter(x=dftn['date'],y=dftn['negativeIncrease']/dftn['totalTestResultsIncrease'],stackgroup='one',name='Negative fraction'),secondary_y=True)
fig_tn.update_yaxes(dict(title_text="Count"))
fig_tn.update_xaxes(dict(title_text='Date'))
fig_tn.update_layout(title="Tennessee COVID-19 Trends",font=dict(color="#7f7f7f"))

print("saving graph")
with open('index.html', 'w') as f:
    f.write(fig_tn.to_html(full_html=False, include_plotlyjs='cdn'))
    # f.write(fig_deaths.to_html(full_html=False, include_plotlyjs='cdn'))


# In[5]:

import matplotlib.pyplot as plt
dftn15 = dftn.iloc[:10]
dftn = dftn15
print(dftn['date'])
# daysbefore = (dftn['date']-pd.Timestamp.today())/pd.Timedelta('1 days')
daysbefore = dftn['date']
fig, ax = plt.subplots(figsize=(8,5))
ax.stackplot(daysbefore,[dftn['positiveIncrease'],dftn['negativeIncrease']], labels=['New Positive cases','New negative tests'])
ax.annotate(str(dftn['positiveIncrease'].iloc[0]),xy=(daysbefore.iloc[0], dftn['positiveIncrease'].iloc[0]),horizontalalignment = 'right')
ax.annotate(str(dftn['negativeIncrease'].iloc[0]),xy=(daysbefore.iloc[0], dftn['negativeIncrease'].iloc[0]),horizontalalignment = 'right')
ax.set_ylabel('Cases',fontsize='x-large')
plt.xticks(rotation=60)
locs, labels = plt.xticks()  # Get the current locations and labels.
# ax.set_xlim(0,1.1)
plt.title('TN recent COVID cases')
ax2=ax.twinx()
ax2.plot(daysbefore, dftn['deathIncrease'],'.-',color='darkseagreen')
ax2.set_ylabel('Deaths',color='darkseagreen',fontsize='x-large')
ax2.annotate(str(dftn['deathIncrease'].iloc[0]),xy=(daysbefore.iloc[0], dftn['deathIncrease'].iloc[0]),horizontalalignment = 'right')
# plt.show()
plt.savefig('pltfig.jpg')


# In[ ]:

with open("index.html", "a") as myfile:
  myfile.write('''<img src="pltfig.jpg" alt="Smiley face">''')

